import numpy as np
import scipy.linalg as nla

# Augmente à 160 le nombre de caractères d'une ligne (-> affichage + sympa)
np.set_printoptions(linewidth=160)

Lambda = np.diag ([2,3,6,8])
X = np.array ([[5,9,7,8],[16,11,34,1],[2,27,24,6],[4,3,12,3]])
Xmu = nla.inv (X)

# On construit A0 à partir de sa diagonalisation (A0 = X . Lambda . X**(-1))
A0 = np.dot (X, np.dot (Lambda, Xmu))
A0 = np.array (A0, dtype=np.float64, order='F')

# Algorithme QR

A = A0
for k in range (0,10) :
    Q, R = nla.qr (A)
    A = np.dot (R, Q)

# Des approximations des valeurs propres sont sur la diagonale de A
mu = A[0,0]

# Version bourrine
v = np.array ([.9742, -.65531, .1111, 3.8], dtype=np.float64)
B = nla.inv (A - mu*np.eye(4))
for k in range (0, 10) :
    v = np.dot (B, v)
    v = (1 / nla.norm(v, 2)) * v

# Quotient de Rayleigh optimisé car v de longueur 1
new_mu = np.dot(v, np.dot (A, v))

# Version moins bourrine
v = np.array ([.9742, -.65531, .1111, 3.8], dtype=np.float64)
M = A - mu*np.eye(4)
P, L, U = nla.lu (M)
for k in range (0, 10) :
    v = np.dot (v, P)  # v = P**T . v
    v = nla.solve_triangular (L, v, lower=True)
    v = nla.solve_triangular (U, v, lower=False)
    v = (1 / nla.norm(v, 2)) * v

# On essaie de reconstruire la diagonalisation

new_Lambda = np.zeros([4,4])
new_X = np.empty ([4,4])
for i in range(0,4) :
    mu = A[i,i]
    # puissance inverse -> v
    v = np.array ([.9742, -.65531, .1111, 3.8], dtype=np.float64)
    M = A - mu*np.eye(4)
    P, L, U = nla.lu (M)
    for k in range (0, 10) :
        v = np.dot (v, P)  # v = P**T . v
        v = nla.solve_triangular (L, v, lower=True)
        v = nla.solve_triangular (U, v, lower=False)
        v = (1 / nla.norm(v, 2)) * v
    new_X[:,i] = v
    new_Lambda[i,i] = np.dot(v, np.dot (A, v)) # le quotient de Rayleigh







