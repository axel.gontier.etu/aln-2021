

#v2_svd sans fonction

from math import sqrt
import math
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt
from numpy import linalg as LA
# Affichage plus agréable
np.set_printoptions(linewidth=240)

# Le document original comporte 15 variables.
# J'en ai retenu 12.

A = np.array ([
    [3, 97.8, 119, 2.6, 7.4, 0.2, 1.8, 2.2, 53, 34, 28.3, 21],
    [0.2, 80.5, 121, 2.1, 5.3, 0.5, 1.4, 1.4, 47.4, 20.5, 27.8, 19.9],
    [0.4, 6.1, 67, 4.2, 9.9, 3, 1.5, 2.5, 10.6, 21, 15.1, 23.1],
    [7.3, 106.4, 129, 1.9, 14.7, 1.1, 2.1, 3.7, 45.9, 12.5, 22.1, 29.9],
    [4.6, 170.6, 79, 1, 26.4, -4.4, 1.4, 11.8, 27.6, 23, 26, 31],
    [6.7, 69.3, 98, 2.4, 26.2, -1.4, 1.4, 4.9, 32.7, 35, 22.7, 27],
    [3.7, 86, 108, 2.2, 10.6, 0.1, 2, 2, 45.4, 34, 30.8, 19.3],
    [2.1, 120.7, 100, 3.3, 11.7, -1, 1.4, 4.6, 35.6, 33, 27.8, 24.5],
    [4.5, 71.1, 94, 3.1, 14.7, -3.5, 1.4, 12, 20.7, 10, 18.4, 23.5],
    [0.9, 18.3, 271, 2.9, 5.3, 0.5, 1.5, 2.3, 50.7, 22, 20.1, 16.8], 
    [2.9, 70.9, 85, 3.2, 7, 1.5, 1.5, 4, 25, 35, 18.9, 21.4],
    [3.6, 65.5, 131, 2.8, 6, -0.6, 1.8, 1.6, 55.7, 34, 28.4, 15.7],
    [2.5, 72.4, 129, 2.6, 4.9, 0.7, 1.4, 1.7, 45.6, 34, 28.2, 16.9],
    [4.9, 108.1, 77, 2.8, 17.6, -1.9, 1.4, 6, 14.8, 25, 24.3, 24.4],
    [5.1, 46.9, 84, 2.8, 10.2, -2, 1.6, 4.9, 18.5, 20, 21.5, 19.3],
    [3.3, 43.3, 73, 3.7, 14.9, 1.1, 1.5, 2.1, 10.1, 19, 16, 20.6],
    [1.5, 49, 114, 3.2, 7.9, 0.3, 1.8, 1.6, 46.8, 26, 26.3, 17.9]],
    dtype=np.float64)

W=np.array([
    [-7/10, -109/25, 1/50 ],
[ 7/10, -31/25, 209/50 ],
[ 23/10, -49/25, 161/50 ],
[ -23/10, -91/25, 49/50 ]],dtype=np.float64)








m, n = A.shape
lignes = ['BE', 'DE', 'EE', 'IE', 'EL', 'ES', 'FR', 'IT', 'CY', 'LU', 'MT', 'NL', 'AT', 'PT', 'SL', 'SK', 'FI']
colonnes = [
'Déficit public / PIB (estimation 2013)', 
'Dette / PIB en %', 
'PIB / habitant', 
'inflation fin 2012', 
'taux de chômage M01 en 2013', 
'taux de croissance du PIB réel (prévision 2013)', 
'taux de fécondité', 
'taux d\'emprunt à 10 ans', 
'coût main d\'œuvre / produits manufacturés', 
'impôt sur les sociétés', 
'prélèvements sociaux', 
'% de pauvreté ou d\'exclusion sociale']


#calcul moyenne des colonnes

l=[]
for i in range(n):#parcours des colonnes
    l.append(A[:,i].mean())
#print(l)
#soustraction de la moyenne de la colonne correspondante
for i in range(n):
    for j in range(m):
        A[j,i]-=l[i]
#print(A)

#adimensionnement des données

lsd=[]
for i in range(n):
    lsd.append(sqrt(np.dot(np.transpose(A[:,i]),A[:,i])/m))
#print(lsd)
print('\n\n')
#division par sd

for i in range(n):
    for j in range(m):
        A[j,i]/=lsd[i]
#print(A)
def reflecteur (m, v) :
    n = v.shape[0]
    F = np.eye (n) - (2*np.outer (v, v))
    Q = np.eye (m, dtype=np.float64)
    Q[m-n:m,m-n:m] = F
    return Q
#calcul valeur singulière

def eigh_tridiagonal(d,e):
    return d,e

def svd(A):
    m,n=A.shape
    B=np.copy(A)
    VL=[]
    VR=[]
    for i in range(n):
        x=B[i:m,i]
        v=np.copy(x)
        v[0]=v[0]+np.sign(v[0])*nla.norm(x,2)
        v=(1/nla.norm(v,2))*v
        VL.append(v)
        Q=reflecteur(m,v)
        B=np.dot(Q,B)
        
        if i<n-2:
            x=B[i,i+1:n]
            v=np.copy(x)
            v[0]=v[0]+np.sign(v[0])*nla.norm(x,2)
            v=(1/nla.norm(v,2))*v
            VR.append(v)
            Q=reflecteur(n,v)
            B=np.dot(B,Q)
            
    #suppresion des m-n dernières ligne de B pour la rendre carré
    B=B[0:n,0:n]
    #construction de T
    H=np.zeros([2*n,2*n],dtype=np.float64)
    H[0:n,n:2*n]=np.transpose(B)
    H[n:2*n,0:n]=B
    P = np.zeros ([2*n,2*n], dtype=np.float64)
    for i in range (0,n) :
        P[i,2*i] = 1
        P[n+i,2*i+1] = 1

    T = np.dot (np.transpose(P), np.dot (H, P))

    d = np.zeros (2*n, dtype=np.float64)
    e = np.array ([ T[i+1,i] for i in range (0,2*n-1) ], dtype=np.float64)
    eigvals, eigvecs = nla.eigh_tridiagonal(d, e)
    print(eigvecs)
    Lambda = eigvals [n:2*n]
    Q = eigvecs [:,n:2*n]
    Y = np.sqrt(2) * np.dot (P, Q)
    print("\n\n")
    newVt = np.transpose (Y[0:n,:])
    newU = np.zeros ([m,n], dtype=np.float64)
    newU[0:n,:] = Y[n:2*n,:]
    newSigma = np.array (np.diag (Lambda), dtype=np.float64)
    for i in range (n-1, -1, -1) :
        Q = reflecteur (m, VL[i])
        newU = np.dot (Q, newU)
    for i in range (n-3, -1, -1) :
        Q = reflecteur (n, VR[i])
        newVt = np.dot (newVt, Q)
    for i in range (1,n) :
        newVt[i,:]=-1*newVt[i,:]
        
    return newVt
#il y a juste les deux dernières lignes qui sont opposés au résultat sur la fiche du projet p10 dou la derniere boucle for dans la fonction svd

print('\n')
print('\n')
#print(svd(A))

u,s,v2t=np.linalg.svd(A, full_matrices=False)
#verif
vt=svd(A)
print('\n')
#print(np.dot(u,np.dot(np.diag(s),vt)))
print('vt =')
#print(vt)
print("\n\n")
#print("vtpython=",v2t,"\n")
#print("python vt =",v2t)
#etape 5 calcul de P
i,j=vt.shape
#v1=vt[0,:]
v1=vt[i-1,:]
v2=vt[i-2,:]
print("\n",v1,"\n",v2)
#v2=vt[1,:]
#avec notre algo svd les valeurs singulières sont par ordre croissant contrairement à l'algo de python donc si on veut
#obtenir les deux vecteurs de vt associé aux plus grandes valeurs singulières il faut récupérer les deux derniers vecteurs de la matrice .
print("\n\n")
v1v2=np.array([v1,v2])
v2=np.array([v1,v2])
P=np.dot(A,np.transpose(v1v2))
#print(P)





plt.scatter(P[:,0],P[:,1])
for i in range(0,m):
    print(P[i,:])
    plt.annotate(lignes[i],P[i,:])
 
plt.scatter (0, 0, color='white')
plt.show()
