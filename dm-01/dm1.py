#def f(x):
#	return (x-2)**2
#def df(x):
#	return 2*x-4
#def f(x):
#	return x*x-3
#def df(x):
#	return 2*x
#calcul racine de 3
#u=3
#print("u0=",u)
#for i in range(5):
#	u=u-(f(u)/df(u))
#	print("u",i+1,"=",u)






#debut question 1

#u=3
#print("u0=",u)
#for i in range(14):
#	z=u-(f(u)/df(u))
#	print((z-2)/(u-2)**2)
#	u=u-(f(u)/df(u))
#	print("u",i+1,"=",u)

#question 2 	
#composante de la matrice jacobienne du système
def f11(x,y):
    return 2*x+2*y
def f12(x,y):
    return 2*x
def f21(x,y):
    return 2*x*y*y
def f22(x,y):
    return 2*x*x*y-1
def fx(x,y):
    return x*x+2*x*y-1
def fy(x,y):
    return x*x*y*y-y-3



import numpy as np
import scipy.linalg as nla

u = np.array([3,-1])
print(u)
for i in range(5):
    J = np.array([[f11(u[0],u[1]),f12(u[0],u[1])],[f21(u[0],u[1]),f22(u[0],u[1])]])
    b = np.array([-fx(u[0],u[1]),-fy(u[0],u[1])])
    #z = nla.solve(J,b)#en utilisant la fonction de scipy
    Q,R = nla.qr(J)	#resolution par la factorisation QR
    z=nla.solve_triangular(R,np.dot(b,Q),lower=False)
    u =  u + z
    print("u",i+1,"=",u)
    

