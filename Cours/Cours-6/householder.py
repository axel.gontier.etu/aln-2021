import scipy.linalg as nla
import numpy as np

A = np.array ([[1,-1,4],[1,4,-2],[1,4,2],[1,-1,0]],dtype=np.float64)
m, n = A.shape
x = A[:,0]
v = x + np.array([nla.norm(x,2), 0, 0, 0])
F = np.eye(4) - (2 / np.dot(v,v)) * np.outer(v,v)
Q1 = F
A2 = np.dot (Q1, A)
x = A2[1:m,1]
v = x + np.array([nla.norm(x,2),0,0])
F = np.eye(3) - (2/np.dot(v,v)) * np.outer(v,v)
Q2 = np.eye(4)
Q2[1:m,1:m] = F
A3 = np.dot (Q2, A2)
# A finir ...
