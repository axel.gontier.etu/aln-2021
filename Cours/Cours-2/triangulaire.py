import numpy as np

A = np.array ([[2,0,0,0],[1,-3,0,0],[5,2,1,0],[1,1,-3,4]], dtype=np.float64)
# Je triche : je construis b à partir de x
# x s'affiche en ligne (c'est un tableau à une dimension)
# C'est nous qu savons que x représente un vecteur colonne
x = np.array ([2,0.5,-4,3], dtype=np.float64)
b = np.dot (A,x)
# Le but du jeu : retrouve x à partir de A et de b

def forward (A, b) :
    m = A.shape[0]
    for i in range (0, m) :
        for j in range (0, i) :
            b[i] = b[i] - A[i,j]*b[j]
            print ('b[',i,'] = b[',i,'] - A[',i,',',j,']*b[',j,']')
        b[i] = b[i]/A[i,i]
        print ('b[',i,'] = b[',i,']/A[',i,',',i,']')

def backward (A, b) :
    m = A.shape[0]
    for j in range (0, m) :
        b[j] = b[j] / A[j,j]
        print ('b[',j,'] = b[',j,']/A[',j,',',j,']')
        for i in range (j+1,m) :
            b[i] = b[i] - A[i,j]*b[j]
            print ('b[',i,'] = b[',i,'] - A[',i,',',j,']*b[',j,']')

# En Python

import scipy.linalg as nla

A = np.array ([[2,0,0,0],[1,-3,0,0],[5,2,1,0],[1,1,-3,4]], dtype=np.float64, order='F')
b = np.dot (A,x)
nla.solve_triangular (A, b, lower=True, overwrite_b=True)
b



